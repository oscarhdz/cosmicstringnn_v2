"""
by Razvan Ciuca, 2017

This file contains only the variables relevant to the rest of the code
to use them, do 'from config import *'  , simply doing 'import config' will not give the desired result


"""


import numpy as np
import torch as t

# ----------------DataFeeder settings-----------------
# here is the location where you placed the cmbedge files used for training the network,
# these are distinct from the ones used for computing the frequencies. This is because during training, a hubble steps
# number of 1 is beneficial: the strings are sparser and this helps the network learn better
# during frequency computation, we want the most accurate simulations possible, and these are the NH = 3 maps
cmbedge_g_training_filename = "../data/cmbedge_g_maps.npy"
cmbedge_s_training_filename = "../data/cmbedge_s_maps.npy"
cmbedge_a_training_filename = "../data/cmbedge_a_maps.npy"

# ----------------training settings--------------------

# basically only used for defaulting, the actual string type for training is in training_string_type_list
global_string_type = 'cmbedge'

# these lists give you the freedom to specify the Gmu, learning rate, number of training steps,
# string type for each training run separately
Gmu_list = [5e-6, 1e-6, 5e-7, 1e-7, 5e-8, 1e-8, 5e-9]
learning_rates_list = [1e-3]*len(Gmu_list)
training_string_type_list = [global_string_type]*len(Gmu_list)
num_steps_per_train_run_list = [5000] + [2000]*(len(Gmu_list)-1) if t.cuda.is_available() else [10]*len(Gmu_list)

noise_list = [None]  # if this list is not [None], it will run, for every Gmu, a run for also each noise value
batch_size = 1 if t.cuda.is_available() else 1
save_model_and_images_every = 10 if t.cuda.is_available() else 1
start_training_from_this_model_filename = None
# if you specify a starting model, the network type will adjust itself to the loaded value, this next variable
# is used only if start_training_from_this_model_filename == None , when starting from scratch.
# Must be 'Net' or some other network type that you can define.
default_network_type = 'Net'
models_directory = '../models'
images_directory = '../images'


# ----------------frequency calculation settings-----------------

frequency_calculation_string_type = 'cmbedge'
compute_frequencies_for_this_model_filename = '../models/modelv2.pth'
# in addition to saving the frequencies in the model, we save them explicitely at the following filename
also_save_frequencies_to = '../data'

cmbedge_g_frequency_calculation_filename = '../data/cmbedge_g_maps_frequency_computation.npy'
cmbedge_s_frequency_calculation_filename = '../data/cmbedge_s_maps_frequency_computation.npy'
cmbedge_a_frequency_calculation_filename = '../data/cmbedge_a_maps_frequency_computation.npy'

# number of total maps for which we compute predictions, if we're on gpu, use all maps
n_maps_used_for_frequency_calculation = 500 if t.cuda.is_available() else 3
batch_size_for_frequency_calculation = 50 if t.cuda.is_available() else 1

# number of pixels to remove from the border, to avoid edge effects from non-periodic maps
# this is used in the frequency calculation and the posterior calculation
border = 30

# parameters used in the frequency calculation. global_f_divisions is number of bins.
global_f_divisions = 1000 if t.cuda.is_available() else 30
global_starting_gmu = 2e-7
global_final_gmu = 1e-10
global_gmu_divisions = 300 if t.cuda.is_available() else 50

# ----------------Bayesian calculation settings-----------------

# whether or not to also compute prediction maps during the bayesian calculation
compute_prediction_maps = True
save_prediction_maps_to = '../prediction_maps/'
a_maps_filename_used_for_bayesian = cmbedge_a_frequency_calculation_filename
# number of maps excluded from the beginning of a_maps, from fear of using the same answer maps as were used to
# produce the candidate sky maps
number_of_a_maps_not_used = 50
posteriors_directory = '../posteriors/'
compute_posteriors_with_this_model_filename = '../models/modelv2.pth'
candidate_maps_directory = '../candidate_sky_maps/'
# list of names of candidate maps for which you wish to compute posteriors, the posteriors will be saved
# in candidate_maps_directory under the same name as the candidate_map, with an appended '_posterior'
candidate_maps_filename_list = [candidate_maps_directory + 'NH_3_Gmu_' + '%.2g' % Gmu + '.npy' for Gmu
                                in [1e-7, 5e-8, 2e-8, 1e-8, 5e-9, 2e-9] ]

# number of Gmu divisions for which to compute posteriors, this doesn't need to be the same as global_gmu_divisions,
# if it is larger than global_gmu_divisions, bayesian_analytic can interpolate between the values of Gmu
N_Gmu_to_plot = 300
# the default plotting limits used are those from the frequency calculation, i.e.
# [global_starting_gmu, global_final_gmu], however, if for some reason you're only interested in another range, you
# have the option of setting it here
overwrite_Gmu_limits = False
forced_smallest_Gmu = 5e-9
forced_largest_Gmu = 1e-7















