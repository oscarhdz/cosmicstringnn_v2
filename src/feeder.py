"""
by Razvan Ciuca, 2017

This script defines a DataFeeder class which load gaussian temperature maps, string temperature maps and
string answer maps, then provides a method for sampling from them. This is mainly used by train.py to get the 
training data

NEW FUNCTIONALITY:

1. now the filenames are not hardcoded, but are defined in config.py, this is cleaner

"""

# Import stuff
import numpy as np
import torch as t
from torch.autograd import Variable
from scipy.misc import imshow
from config import *


# This class handles the feeding of data, i.e. it handles loading the relevant files, calling the scripts which
# produce the numpy versions of the maps, it handles combining the data into a format appropriate for feeding
# to the network for training.
class DataFeeder:

    # The initialization routine, when instantiating a DataFeeder using "feeder = DataFeeder()", this function is run,
    # This initial function checks that all necessary files exist.
    def __init__(self, cmbedge_s_filename=cmbedge_s_training_filename,
                       cmbedge_a_filename=cmbedge_a_training_filename,
                       cmbedge_g_filename=cmbedge_g_training_filename,
                       load_string_type="cmbedge"):

        # Starting to load the files from filenames, we allow for files not existing, in which case we also make them
        # by calling the appropriate scripts
        print("starting to load numpy files into DataFeeder")
        if True:
            # load gaussian maps

            if load_string_type == 'cmbedge':
                try:
                    self.cmbedge_g_maps = np.load(cmbedge_g_filename)
                except FileNotFoundError:
                    print("the file " + cmbedge_g_filename + " does not exist.")
                self.cmbedge_g_maps = self.cmbedge_g_maps / self.cmbedge_g_maps.std()
            else:
                print('load_string_type for g_maps must be cmbedge')

            # load string temperature maps

            if load_string_type == 'cmbedge':
                try:
                    self.cmbedge_s_maps = np.load(cmbedge_s_filename)
                except FileNotFoundError:
                    print("the file " + cmbedge_s_filename + " does not exist.")
            else:
                print('load_string_type for s_maps must be cmbedge')

            # load answer maps

            if load_string_type == 'cmbedge':
                try:
                    self.cmbedge_a_maps = np.load(cmbedge_a_filename)
                except FileNotFoundError:
                    print("the file " + cmbedge_a_filename + " does not exist.")
            else:
                print('load_string_type for a_maps must be cmbedge')

        print("finished loading files")
        # we normalize the gaussian maps to have unit standard deviation, in get_batch we'll multiply by the
        # appropriate standard deviation
        self.map_shape = self.cmbedge_g_maps.shape
        self.cmbedge_g_maps = self.cmbedge_g_maps/self.cmbedge_g_maps.std()

    # alpha factor which multiplies the gaussian maps when producing full maps at a given Gmu
    def alpha(self, Gmu):
        return np.sqrt(1.0 - 3.3873e10 * Gmu * Gmu)

    # function which translates maps by (t_x, t_y) assuming periodic boundary conditions
    # this is used in the data-augmentation step, i.e. when sampling from the maps, we have the option to
    # generate a random translation vector and apply it, this increases the effective number of possible maps
    def translate_maps(self, maps, t_x, t_y):
        slice_1 = np.concatenate([maps[:, t_x:, t_y:], maps[:, t_x:, :t_y]], 2)
        slice_2 = np.concatenate([maps[:, :t_x, t_y:], maps[:, :t_x, :t_y]], 2)

        return np.concatenate([slice_1, slice_2], 1)

    # this is the most important function of the DataFeeder object, it samples some number of maps from the
    # datasets, combines the string components with the gaussian components using the given Gmu and formats everything
    # in the way which pytorch expects
    def get_batch(self, batch_size=1, Gmu=1e-7, noise=None, random_indices=False, gpu_flag=False, string_type=global_string_type):

        # don't try to get more maps than we have
        if batch_size > self.map_shape[0]:
            print("batch size bigger than available data, exiting")
            return

        # we select a sub-array of size batch_size from the s_maps, a_maps and g_maps
        g_start_index = 0
        s_start_index = 0

        if random_indices:
            g_start_index = np.random.choice(self.map_shape[0] - batch_size)
            s_start_index = np.random.choice(self.map_shape[0] - batch_size)

        if string_type == 'cmbedge':
            s_maps_batch = self.cmbedge_s_maps[s_start_index:s_start_index + batch_size]
            a_maps_batch = self.cmbedge_a_maps[s_start_index:s_start_index + batch_size]
            g_maps_batch = self.cmbedge_g_maps[g_start_index:g_start_index + batch_size]
        else:
            print('string_type must be cmbedge')

        if random_indices:
            t_x = np.random.choice(500)
            t_y = np.random.choice(500)
            g_maps_batch = self.translate_maps(g_maps_batch, t_x, t_y)

        # 1 microK = 0.02208

        # this combines the maps to produce the full-sky maps, if the string type is ringeval,
        # we multiply the g_maps_batch by 3.697e-5, this is because we earlier set the standard deviation of the
        # gaussian maps to 1. We did this because our cmbedge gaussian maps have a different standard
        # deviation than the ringeval gaussian maps (which have std=100), so we need to rescale the ringeval files to match ours
        out_maps_batch = g_maps_batch * (3.697e-5 * self.alpha(Gmu)) + s_maps_batch * Gmu

        # if noise is a floating point value, add gaussian noise with standard deviation equal to noise
        if noise is not None:
            out_maps_batch += noise * np.std(out_maps_batch) * np.random.randn(batch_size, 1, out_maps_batch.shape[2], out_maps_batch.shape[3])

        # we normalize the final maps by dividing by the standard deviation, this is because neural nets behave better
        # when we feed them inputs of order 1, feeding them a map with std=1e-5 would make it hard for the training to
        # converge
        out_maps_batch /= np.std(out_maps_batch)

        # finally, we convert everything from numpy to pytorch formats, if a gpu is detected, it sends the batch to
        # gpu memory
        if gpu_flag or t.cuda.is_available():
            return Variable(t.from_numpy(out_maps_batch).cuda()), Variable(t.from_numpy(a_maps_batch).cuda())
        else:
            return Variable(t.from_numpy(out_maps_batch)), Variable(t.from_numpy(a_maps_batch))

# if you are calling this one directly, it must be because of troubleshooting, so here's a piece of code which
# shows roughly how to use the code in this script
if __name__ == '__main__':
    # create a feeder
    feeder = DataFeeder()

    # example of how to produce a batch of training maps from the feeder
    # inputs and answers are [5, 1, 512, 512] pytorch tensors encased in Variable objects,
    # if gpu_flag is set to True or pytorch detects a gpu to be avaiable, these maps will be in gpu memory
    inputs, answers = feeder.get_batch(batch_size=5, Gmu=2e-6, noise=None, random_indices=False,
                                       gpu_flag=False, string_type='cmbedge')

    # this will show the first image of the batch, notice that inputs here is a Variable pytorch tensor, so get it
    # into the numpy format necessary for imshow, we need to access the .data field, then call the .numpy()
    # method, then access the [0, 0] component, which will yield a [512, 512] numpy array
    imshow(inputs.data.numpy()[0, 0])

    # also showing the corresponding answer map
    imshow(answers.data.numpy()[0, 0])


