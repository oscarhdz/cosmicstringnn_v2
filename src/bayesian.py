"""
by Razvan Ciuca, 2017



"""

import torch as t
from torch.autograd import Variable
import torch.nn.functional as F
import torch.nn as nn
import numpy as np
import numpy.fft as fft
import pickle
import scipy.misc
import os
import sys

from feeder import DataFeeder
from model_def import Net, load_model
from utils import *
from compute_frequencies import compute_frequencies
from config import *


#
def bayesian_posterior_2(sky_map, model, histograms, frequencies, write_to=None):

    # evaluate the model on the sky_map and convert to numpy
    pred = t.sigmoid(model.forward(Variable(sky_map/sky_map.std())))
    pred = pred.squeeze()[border:-border, border:-border].data.cpu().numpy()

    # uncomment line below to show the prediction
    # scipy.misc.imshow(pred)

    # fancy way of getting back the histogram_divisions variable defined in compute_frequency
    histogram_divisions = len(histograms[list(histograms.keys())[0]][0])

    pred_hist, pred_intervals = np.histogram(pred, histogram_divisions, range=(0.0, 1.0))

    epsilon = 2
    posterior = {}

    for Gmu in sorted(histograms.keys()):

        means, stds, intervals = histograms[Gmu]
        posterior[Gmu] = 0

        for i in range(0, len(means)):
            posterior[Gmu] += (pred_hist[i] - means[i])**2/(stds[i] + epsilon)

        posterior[Gmu] /= -len(means)

    # we will subtract the posterior mean for convenience, since we are computing log(posterior), this amounts to
    # an irrelevant probability normalisation factor
    posterior_mean = np.mean(list(posterior.values()))

    print("mean of log posteriors is " + str(posterior_mean))

    if write_to is not None:
        with open(write_to, 'w') as file:
            for Gmu in sorted(frequencies.keys()):
                file.write(str(Gmu) + ' ' + str(posterior[Gmu] - posterior_mean ) + '\n')

    return posterior


# this returns a numpy map corresponding to P(xi | sky_map, Gmu)
def get_conditioned_prediction_map(pred_maps, conditional_Gmu):

    sorted_Gmu = sorted(pred_maps.keys())[::-1]

    if conditional_Gmu > sorted_Gmu[0]:
        print('conditional Gmu too large, it needs to be smaller than ' + str(sorted_Gmu[0]))
    elif conditional_Gmu < sorted_Gmu[-1]:
        print('conditional Gmu too small, it needs to be larger than ' + str(sorted_Gmu[-1]))

    for i in range(0, len(sorted_Gmu)):

        if conditional_Gmu < sorted_Gmu[i] and conditional_Gmu > sorted_Gmu[i+1]:

            p = (conditional_Gmu - sorted_Gmu[i])/(sorted_Gmu[i+1]-sorted_Gmu[i])

            return (pred_maps[sorted_Gmu[i]]*(1-p) + pred_maps[sorted_Gmu[i+1]]*p).squeeze()


# computes the rescaled prediction maps, the output is a dictionary, the keys are Gmus and maps[Gmu] contains
# P(\xi | \delta_sky, G\mu)
# only for this Gmu functionality added
def prediction_maps(sky_map, model, frequencies, prior):

    prediction = model.forward(Variable(sky_map)).data.cpu().numpy()

    maps = {}

    for Gmu in frequencies.keys():
        # print("done Gmu " + str(Gmu))
        intervals, freq = frequencies[Gmu]

        maps[Gmu] = np.zeros([1, 1, 512, 512])

        # first take care of all the pixels outside the range of intervals

        # set all pixels outside this range to the prior
        maps[Gmu][prediction < intervals[0]] = prior
        maps[Gmu][prediction >= intervals[-1]] = prior

        for i in range(0, len(intervals) - 1):

            # q is the true positive rate of the pixels between interval[i] and interval[i+1]
            q = freq[i]
            counter = 0
            while np.isnan(q):
                counter += 1
                q = freq[i-counter]

            maps[Gmu][(prediction >= intervals[i]) * (prediction < intervals[i+1])] = q

        maps[Gmu][maps[Gmu] <= 0.001] = prior

    return maps


# main loop, for evaluating a list of maps at a time
def bayesian_main():
    # create the posteriors folder if it doesn't already exist
    os.system('mkdir ' + posteriors_directory)
    os.system('mkdir ' + save_prediction_maps_to)
    model = load_model(compute_posteriors_with_this_model_filename)
    if t.cuda.is_available():
        model = model.cuda()

    histograms = pickle.load(open(also_save_frequencies_to + '/histograms_' + frequency_calculation_string_type, 'rb'))
    frequencies, prior = pickle.load(open(also_save_frequencies_to + '/frequencies_' + frequency_calculation_string_type, 'rb'))

    for sky_map_filename in candidate_maps_filename_list:

        print("computing posteriors for " + sky_map_filename)

        sky_map = np.load(sky_map_filename).reshape([1, 1, 512, 512])
        sky_map = t.from_numpy(sky_map).cuda() if t.cuda.is_available() else t.from_numpy(sky_map)
        sky_map /= sky_map.std()

        #OH
        write_to_prefix = posteriors_directory + sky_map_filename.split('/')[-1].split('.npy')[0] #+ '_posterior'

        # finally compute the posterior and write it to the given file
        posterior = bayesian_posterior_2(sky_map, model, histograms, frequencies, write_to=write_to_prefix)

        # -------- Now computing prediction maps ---------

        # next we compute the prediction maps:
        if compute_prediction_maps:

            print("computing and saving prediction maps")
            prediction_map_write_to = save_prediction_maps_to + sky_map_filename.split('/')[-1].split('.npy')[0]

            # this is a dictionary for which pred_maps[Gmu] contains the map of values P(\xi | \delta_sky, G\mu)
            # the keys of the dictionary are the keys of the frequencies dictionary
            pred_maps = prediction_maps(sky_map, model, frequencies, prior)
            pickle.dump(pred_maps, open(prediction_map_write_to, 'wb'))

            # now pick the maximal map and save it as an image:
            # this line is just a fancy way to extract the argmax of the posterior
            max_posterior_gmu = max(posterior.keys(), key=(lambda key: posterior[key]))

            # save the image using imsave, the name contains the argmax of the posterior
            scipy.misc.imsave(prediction_map_write_to + '_max_posterior_gmu_%.2e.png' % max_posterior_gmu,
                              pred_maps[max_posterior_gmu].squeeze())

if __name__ == '__main__':
    bayesian_main()
