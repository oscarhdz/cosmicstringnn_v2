"""
by Razvan Ciuca, 2017

This script simply creates a bunch of sky_maps with different Gmus and NH=3 and saves them to ../candidate_sky_maps

"""

import os
import numpy as np
import torch as t
from feeder import DataFeeder
from utils import normalize_map
from config import *

g_maps = np.load(cmbedge_g_frequency_calculation_filename)
s_maps = np.load(cmbedge_s_frequency_calculation_filename)

os.system('mkdir ' + candidate_maps_directory)

for Gmu in [1e-7, 5e-8, 2e-8, 1e-8, 5e-9, 2e-9]:

    maps = normalize_map(g_maps + Gmu * s_maps)
    sky_map = maps[0, 0]
    np.save(candidate_maps_directory + 'NH_3_Gmu_' + str(Gmu), sky_map)



