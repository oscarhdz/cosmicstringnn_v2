The code CosmicStringNN is a convolutional neural network for detecting cosmic strings in CMB maps.  Its use is described in arXiv:1706.04131 and arXiv:1708.08878 . If you use any part of the code for a publication or e-print please reference the above arXive e-prints and the code's online location: https://gitlab.com/oscarhdz/cosmicstringnn_v2/
The code CosmicStringNN has been significantly improved and the improvement is described in arXiv:1810.11889 . When the improved code is released publicly, it will be announced on the arXiv. 
Razvan Ciuca, Oscar Hernandez, 2018

DEPENDENCIES--------------------------------------------------------------------
python >= 3.5
numpy
scipy
matplotlib
pytorch 0.3.1 (to install with conda run "conda install pytorch=0.3.1 torchvision -c soumith", otherwise see pytorch.org)
cuda >= 7.5 for GPU. The program will automatically use it if it is available.

FILES_in_src--------------------------------------------------------------------
config.py : 
Contains the settings on variables relevant to the rest of the code. For example, we set the network model to be models/modelv2.pth and the list of string tensions to study is set to be [1e-7, 5e-8, 2e-8, 1e-8, 5e-9, 2e-9]. This should be the only file you need to edit. 

model_def.py :
The definition of the network model.

train.py :
Use this to train the network from scratch or to continue training. A GPU is required train a model from scratch to produce good predictions, however, the program will run even if a GPU is not available. To train the network model defined in src/model_def.py with a dataset of simulated maps located in data/. The simulated maps include cmbedge_g_maps.npy, cmbedge_s_maps.npy, cmbedge_a_maps.npy. These are the gaussian temperature, string temperature, and string answer maps, respectively. They are loaded when train.py calls feeder.py. Running train.py can start from scratch. Or train.py can start with the weights saved in a file in models/ which is specified in config.py.  It will then produce new weights and save them in models/cmbedge/... We have set the model to be models/modelv2.pth in the config.py. This is our trained network model with which we calculated the posteriors and prediction maps in arXiv:1706.04131. During training, images of the answer map and prediction maps are recorded in the directory images/ so that a human can judge how well the training has advanced. 

feeder.py :
Defines a DataFeeder class which load gaussian temperature maps, string temperature maps and string answer maps, then provides a method for sampling from them. This is mainly used by train.py to get the training data.

create_candidate_sky_maps.py :
Creates test maps, simulated sky_maps with multiple Gmus which are unknown to bayesian.py and places them in the directory create_candidate_sky_maps/.

compute_frequencies.py :
This computes and saves the values needed to rescale the raw prediction maps to produce the final prediction maps. The frequencies are saved in data/frequencies_cmbedge . 

bayesian.py :
Given a candidate simulated sky_map with unknown Gmu it creates the prediction map and uses it to calculate the posterior distribution. To compute posteriors on multiple candidate maps only this step needs to be repeated. The prediction maps are placed in the director prediction_maps/ and the posteriors in the directory posteriors/

utils.py :
Miscellaneous utilities used in the code. 

TO_TRAIN_THE_NETWORK------------------------------------------------------------
python train.py
A GPU is needed to train a network model from scratch. 

TO_PRODUCE_POSTERIORS_AND_PREDICTION_MAPS---------------------------------------
We provide a trained model in models/modelv2.pth. This is the trained network model with which we calculated the posteriors and prediction maps in arXiv:1706.04131. If you wish to calculate the posteriors and prediciton maps using this trained network model there is no need to run 'python train.py'. Just run:
0. cd src/
1. python create_candidate_sky_maps.py
2. python compute_frequencies.py
3. python bayesian.py
4. python run_to_see_posteriors_predictions_maps.py
The above steps can be run on a laptop CPU. However, if compute_frequencies.py is run on a CPU, much fewer maps will be used to compute the frequencies and the results, while acceptable, will lead to less precise prediction maps and hence less precise posteriors. Running the other steps on a CPU does not effect the precision.

TO_PLOT_POSTERIOR_PROBABILITIES_AND_SEE_PREDICTION_MAPS-------------------------
python run_to_see_posteriors_predictions_maps.py
