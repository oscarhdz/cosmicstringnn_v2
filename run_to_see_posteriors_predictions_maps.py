# Plotting Posteriors and Seeing Prediction Maps, Oscar Hernandez 2018
#
# After running the program as described in the README,
# this script plots the posterior in the directory posteriors/
# and generates images of the prediction maps in the directory prediction_maps/
# Edit the gmu_str_list and gmu_str_list_pred below to plot and see what you want. 

import matplotlib.pyplot as plt
import sys
from sympy import *
import numpy as np
import pickle

def normalize(log_posterior): #this is a pointer to the original array
    gmu=log_posterior[:,0] 
    len_pos=len(gmu)
    
    #integrate trapezoidal rule with sympy exp since numpy arrays have a sys.float_info.max of 1.8e308. 
    norm=0
    for x in range(0,len_pos-1):
        exp_posterior_x = exp( log_posterior[x][1] )
        exp_posterior_x_plus_1 = exp( log_posterior[x+1][1] )
        norm += (1/2)* (exp_posterior_x_plus_1 + exp_posterior_x) * (gmu[x+1]-gmu[x])
        norm = abs( norm ) #because the gmu list can be backwards with large gmu at the beginning

    ln_norm = log(norm)
    print("norm=",norm, "ln_norm=",ln_norm)
    log_posterior[:,1]=log_posterior[:,1]-ln_norm
    return

prefix='posteriors/NH_3_Gmu_'
gmu_str_list=['1e-07','5e-08','2e-08','1e-08','5e-09','2e-09'] 
logP=[]
for gmu in gmu_str_list:
    logP.append(np.loadtxt(prefix + gmu) )
for j in range(0,len(gmu_str_list)):
    normalize(logP[j])
    plt.semilogx(logP[j][:,0], logP[j][:,1])
#plt.xlim(1.0e-10,5e-7)
plt.ylim(-200,50)
plt.grid(True)
plt.show()

# ## Seeing Prediction Maps
# this returns a numpy map corresponding to P(xi | sky_map, Gmu)
def get_conditioned_prediction_map(pred_maps, conditional_Gmu):

    sorted_Gmu = sorted(pred_maps.keys())[::-1]
    if conditional_Gmu > sorted_Gmu[0]:
        print('conditional Gmu too large, it needs to be smaller than ' + str(sorted_Gmu[0]))
    elif conditional_Gmu < sorted_Gmu[-1]:
        print('conditional Gmu too small, it needs to be larger than ' + str(sorted_Gmu[-1]))

    for i in range(0, len(sorted_Gmu)):
        if conditional_Gmu < sorted_Gmu[i] and conditional_Gmu > sorted_Gmu[i+1]:
            p = (conditional_Gmu - sorted_Gmu[i])/(sorted_Gmu[i+1]-sorted_Gmu[i])
            return (pred_maps[sorted_Gmu[i]]*(1-p) + pred_maps[sorted_Gmu[i+1]]*p).squeeze()

prefix='prediction_maps/'
gmu_str_list_pred=['1e-07','5e-08','2e-08','1e-08','5e-09','2e-09'] 
for gmu in gmu_str_list:
    print(gmu)
    fileObject = open(prefix+'NH_3_Gmu_'+gmu,'rb')
    preds_rescaled=pickle.load(fileObject)
    fileObject.close()
    plt.imshow(get_conditioned_prediction_map(preds_rescaled, float(gmu) ),'gray')
    plt.show()

